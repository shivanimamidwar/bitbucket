resource "bitbucket_repository" "bb_repository" {
  
  project_key = var.project_key
  workspace = var.workspace
  name = var.repo_name
  description = "DS User repository"
  enable_pipelines = true
  
}
